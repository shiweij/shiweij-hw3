# shiweij-hw3

[Part 1 reference - tutorial](https://github.com/mininet/mininet/wiki/Introduction-to-Mininet)

    #!/usr/bin/python

	from mininet.topo import Topo
	from mininet.net import Mininet
	from mininet.node import CPULimitedHost
	from mininet.link import TCLink
	from mininet.util import dumpNodeConnections
	from mininet.log import setLogLevel

	class SingleSwitchTopo( Topo ):
	    "Single switch connected to n hosts."
	    def build( self, n=2 ):
		switch = self.addSwitch( 's1' )
		for h in range(n):
		    # Each host gets 50%/n of system CPU
		    host = self.addHost( 'h%s' % (h + 1),
			                 cpu=.5/n )
		    # 10 Mbps, 5ms delay, 2% loss, 1000 packet queue
		    self.addLink( host, switch, bw=10, delay='5ms', loss=2,
	                          max_queue_size=1000, use_htb=True )

	def perfTest():
	    "Create network and run simple performance test"
	    topo = SingleSwitchTopo( n=4 )
	    net = Mininet( topo=topo,
		           host=CPULimitedHost, link=TCLink )
	    net.start()
	    print "Dumping host connections"
	    dumpNodeConnections( net.hosts )
	    print "Testing network connectivity"
	    net.pingAll()
	    print "Testing bandwidth between h1 and h4"
	    h1, h4 = net.get( 'h1', 'h4' )
	    net.iperf( (h1, h4) )
	    net.stop()

	if __name__ == '__main__':
	    setLogLevel( 'info' )
	    perfTest()

[Part 1 reference - API](http://mininet.org/api/classmininet_1_1net_1_1Mininet.html#a2fe3040d8714f0357b9658820770ee62)


    def __getitem__
 		net[ name ] operator: Return node with given name 


[Part 2 reference]()

[Part 3 reference]()